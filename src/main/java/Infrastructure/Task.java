package Infrastructure;

public class Task {
    private String url;
    private String fileName;

    public Task(String url, String fileName) {
        this.url = url;
        this.fileName = fileName;
    }

    public String getUrl() {
        return url;
    }

    public String getFileName() {
        return fileName;
    }
}
