package Infrastructure;

import Infrastructure.Enums.ErrorCode;
import Infrastructure.Errors.Error;

import java.io.BufferedReader;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.server.ExportException;
import java.util.ArrayList;

public class Source {
    private BufferedReader br;
    private Path path;

    public Source(String filePath) {
        try {
            path = Paths.get(filePath);
            br = Files.newBufferedReader(path);
        } catch (Exception e) {
            throw Error.getError(
                    ErrorCode.COULD_NOT_OPEN_FILE,
                    String.format("file `%s` due %s", filePath, e)
            );
        }
    }
    public Task next() {
        try {
            String line = br.readLine();
            if (line == null) {
                return null;
            }
            String[] data = line.split(" ");
            return new Task(data[0], data[1]);

        } catch (Exception e) {
            throw Error.getError(
                    ErrorCode.COULD_NOT_READ_FILE,
                    String.format("file `%s` due %s", path.toString(), e)
            );
        }
    }

}
