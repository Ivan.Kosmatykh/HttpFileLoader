package Infrastructure.Enums;


import java.util.HashMap;
import java.util.Map;

public enum ErrorCode {
    INTERNAL_ERROR,
    URL_COULD_NOT_BE_OPENED,
    INVALID_URL_FORMAT,
    COULD_NOT_OPEN_FILE,
    COULD_NOT_READ_FILE
    ;

    final static public Map<ErrorCode, String> errorMessages = new HashMap<ErrorCode, String>();

    public String getMessage() {
        return ErrorCode.errorMessages.get(this);
    }

    static {
        ErrorCode.errorMessages.put(INTERNAL_ERROR, "Internal error");
        ErrorCode.errorMessages.put(URL_COULD_NOT_BE_OPENED, "Could not open web resource");
        ErrorCode.errorMessages.put(INVALID_URL_FORMAT, "Invalid url format");
        ErrorCode.errorMessages.put(COULD_NOT_OPEN_FILE, "Could not open file");
        ErrorCode.errorMessages.put(COULD_NOT_READ_FILE, "Could not read file");
    }
}
