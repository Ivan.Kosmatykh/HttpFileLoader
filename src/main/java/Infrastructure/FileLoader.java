package Infrastructure;

import Infrastructure.Enums.ErrorCode;
import Infrastructure.Errors.Error;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;


public class FileLoader {
    private int chunkSize;

    public FileLoader(int chunkSize) {
        setChunkSize(chunkSize);
    }

    private void setChunkSize(int chunkSize) {
        this.chunkSize = chunkSize;
    }

    public long load(String fileUrl, String destinationFile) {
        long size = 0;

        try {
            URL url = new URL(fileUrl);

            InputStream source;

            try {
                source = url.openStream();
            } catch (IOException e) {
                throw Error.getError(
                        ErrorCode.URL_COULD_NOT_BE_OPENED,
                        String.format("%s is unreachable due %s", fileUrl, e)
                );
            }

            Path path = Paths.get(destinationFile);

            try {
                BufferedWriter writer  = Files.newBufferedWriter(path);
                int length;
                byte[] data = new byte[chunkSize];
                while ((length = source.read(data)) != -1) {
                    if (length < chunkSize) {
                        data = Arrays.copyOf(data, length);
                    }
                    writer.write(new String(data));

                    size += data.length;
                }

            } catch (IOException e) {
                throw Error.getError(
                        ErrorCode.INTERNAL_ERROR,
                        String.format("could not open file %s due %s", path.getFileName(), e)
                );
            }

        } catch (MalformedURLException e) {
            throw Error.getError(ErrorCode.INVALID_URL_FORMAT, fileUrl);
        }

        return size;
    }
}
