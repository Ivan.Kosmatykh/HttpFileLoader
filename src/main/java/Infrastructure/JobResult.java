package Infrastructure;

public class JobResult {
    protected long time;
    protected long size;
    private String name;

    public JobResult(long time, long size, String name) {
        this.time = time;
        this.size = size;
        this.name = name;
    }

    public long getTime() {
        return time;
    }

    public long getSize() {
        return size;
    }



    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return String.format("name `%s`; time `%s` minutes; size `%s`",
                this.name, this.time/(1000*60), this.size);
    }
}
