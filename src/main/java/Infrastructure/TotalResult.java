package Infrastructure;

public class TotalResult extends JobResult {
    private int counter;

    public TotalResult() {
        super(0, 0, "Total");
    }

    public void addResult(JobResult result) {
        time += result.getTime();
        size += result.getSize();
        counter += 1;
    }

    @Override
    public String toString() {
        return String.format("count %s\n%s\n%s Mb/s", counter, super.toString(), (size/(1024.0*1024.0))/(time/1000.0));
    }
}
