package Infrastructure.Errors;

import Infrastructure.Enums.ErrorCode;

public class Error extends RuntimeException {

    public Error(String message) {
        super(message);
    }

    public Error(String message, String details) {
        super(String.format("%s: %s", message, details));
    }

    public static Error getError(ErrorCode errorCode) {
        String message = errorCode.getMessage();

        if (message == null) {
            throw new RuntimeException(String.format("%s error was not registered", errorCode));
        }
        return new Error(message);
    }

    public static Error getError(ErrorCode errorCode, String details) {
        String message = errorCode.getMessage();

        if (message == null) {
            throw new RuntimeException(String.format("%s error was not registered", errorCode));
        }
        return new Error(message, details);
    }
}
