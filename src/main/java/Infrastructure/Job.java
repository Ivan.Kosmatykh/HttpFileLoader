package Infrastructure;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Job {
    private String url;
    private Path path;

    public Job(String url, String filePath) {
        this.url = url;
        this.path = Paths.get(filePath);
    }

    public JobResult loadFile(FileLoader fl) {
        long time = java.lang.System.currentTimeMillis();
        long size = fl.load(this.url, this.path.toString());
        return new JobResult(
                java.lang.System.currentTimeMillis() - time,
                size, this.path.getFileName().toString()
        );
    }
}
