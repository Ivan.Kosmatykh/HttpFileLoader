package Application;

import Infrastructure.*;

import java.nio.file.Paths;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application {
    private Config config;
    private Source source;

    public Application(Config config) {
        this.config = config;
        this.source = new Source(config.getSourceFile());
    }

    public void run() {
        ExecutorService executor = Executors.newFixedThreadPool(config.getWorkersCount());
        Task task;

        TotalResult totalResult = new TotalResult();
        FileLoader fileLoader = new FileLoader(4096);

        HashSet<String> loaded = new HashSet<>();

        while ((task=source.next()) != null) {
            final Task t = task;
            if (loaded.contains(t.getUrl())) {
                continue;
            }
            loaded.add(t.getUrl());

            executor.submit(()-> {
                System.out.println(String.format("Loading %s", t.getFileName()));
                Job job = new Job(t.getUrl(),
                        Paths.get(config.getDestinationFolder(), t.getFileName()).toString()
                );
                JobResult result = job.loadFile(fileLoader);

                totalResult.addResult(result);

                System.out.println(String.format("Loaded: %s", result));
            });
        }

        executor.shutdown();
        while (!executor.isTerminated()) {
        }

        System.out.println("Loaded 100%");
        System.out.println(totalResult);
    }
}
