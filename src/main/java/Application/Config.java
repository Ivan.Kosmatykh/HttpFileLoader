package Application;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.JCommander;


public class Config {
    @Parameter(names={"--workersCount", "-wc"})
    private int workersCount;

    @Parameter(names={"--sourceFile", "-sf"})
    private String sourceFile;

    @Parameter(names={"--destinationFolder", "-df"})
    private String destinationFolder;

    public int getWorkersCount() {return workersCount;}
    public String getSourceFile() {return sourceFile;}
    public String getDestinationFolder() {return destinationFolder;}

    static public Config create(String[] argv) {
        Config config = new Config();
        JCommander.newBuilder()
                .addObject(config)
                .build()
                .parse(argv);
        return config;
    }

}
