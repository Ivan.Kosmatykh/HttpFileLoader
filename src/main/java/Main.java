import Application.Application;
import Application.Config;

public class Main {
    public static void main(String[] argv) {

        Application app = new Application(Config.create(argv));
        app.run();
    }
}
